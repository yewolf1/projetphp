<?php 
namespace modele\metier;

/* Visibilité des propriétés et méthodes
    public : 		n'importe qui a accès à la méthode ou à l'attribut demandé.
	protected : 	seule la classe ainsi que ses sous classes éventuelles 
                    (classes héritées).
	private : 		seule la classe ayant défini l'élément peut y accéder.
*/
class Chanteur { 
	
    // Constantes de la classe Chanteur
    private const CR = "\n";

    // Attributs d'instance de la classe Chanteur
    // private : pas d'accès à ces attributs en dehors de la classe
    private $id;
    private $nom; 
    private $prenom; 
    private $descriptionUser;
    private $imageUser;

    /** 
    * Cette méthode un peu spéciale est le constructeur
    * Elle est exécutée lorsque vous créez un objet Utilisateur. 
    */ 
    public function __construct() 
    { 
    } 

    // Méthode setter de l'attribut $id
    public function setId($id) : void {
        $this->id = $id;
    }

    // Méthode getter de l'attribut $id
    public function getId() {
        return $this->id;
    }

    // Méthode setter de l'attribut $nom
    public function setNom($nom) : void {
        $this->nom = $nom;
    }

    // Méthode getter de l'attribut $nom
    public function getNom() {
        return $this->nom;
    }

    // Méthode setter de l'attribut $prenom
    public function setPrenom($prenom) : void {
        $this->prenom = $prenom;
    }

    // Méthode getter de l'attribut $prenom
    public function getPrenom() {
        return $this->prenom;
    }

    // Méthode setter de l'attribut $description
    public function setDescriptionUser($descriptionUser) : void {
        $this->descriptionUser = $descriptionUser;
    }

    // Méthode getter de l'attribut $description
    public function getDescriptionUser() {
        return $this->descriptionUser;
    }

    // Méthode setter de l'attribut $imageUser
    public function setImageUser($imageUser) : void {
        $this->imageUser = $imageUser;
    }

    // Méthode getter de l'attribut $imageUser
    public function getImageUser() {
        return $this->imageUser;
    }
	
    /**  
    * Destructeur, appelé quand l'objet est détruit
    */  
    public function __destruct()  
    {  
    	// Libérer les ressources
        // unset() détruit la ou les variables dont le nom a été passé en argument
    	// unset($this->nom);
       	// unset($this->prenom);
       	// unset($this->mail);   	
    }
    
}
?>