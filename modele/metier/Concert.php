<?php 
namespace modele\metier;

/* Visibilité des propriétés et méthodes
    public : 		n'importe qui a accès à la méthode ou à l'attribut demandé.
	protected : 	seule la classe ainsi que ses sous classes éventuelles 
                    (classes héritées).
	private : 		seule la classe ayant défini l'élément peut y accéder.
*/
class Concert { 
	
    // Constantes de la classe Chanteur
    private const CR = "\n";

    // Attributs d'instance de la classe Chanteur
    // private : pas d'accès à ces attributs en dehors de la classe
    private $image;
    private $singer; 
    private $date; 
    private $description;

    /** 
    * Cette méthode un peu spéciale est le constructeur
    * Elle est exécutée lorsque vous créez un objet Utilisateur. 
    */ 
    public function __construct() 
    { 
    } 

    // Méthode setter de l'attribut $id
    public function setImageConcert($image) : void {
        $this->image = $image;
    }

    // Méthode getter de l'attribut $id
    public function getImageConcert() {
        return $this->image;
    }

    // Méthode setter de l'attribut $nom
    public function setSingerConcert($singer) : void {
        $this->singer = $singer;
    }

    // Méthode getter de l'attribut $nom
    public function getSingerConcert() {
        return $this->singer;
    }

    // Méthode setter de l'attribut $prenom
    public function setDateConcert($date) : void {
        $this->date = $date;
    }

    // Méthode getter de l'attribut $prenom
    public function getDateConcert() {
        return $this->date;
    }

    // Méthode setter de l'attribut $description
    public function setDescriptionConcert($description) : void {
        $this->description = $description;
    }

    // Méthode getter de l'attribut $description
    public function getDescriptionConcert() {
        return $this->description;
    }
	
    /**  
    * Destructeur, appelé quand l'objet est détruit
    */  
    public function __destruct()  
    {  
    	// Libérer les ressources
        // unset() détruit la ou les variables dont le nom a été passé en argument
    	// unset($this->nom);
       	// unset($this->prenom);
       	// unset($this->mail);   	
    }
    
}
?>