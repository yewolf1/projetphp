<?php 
namespace modele\dao;

// Inclure le fichier des constantes : il contient notamment la constante LOGFILE
//include(__DIR__.'/../../Constantes.php');

// Classe de gestion des accès à la base de données pour la table T_UTILISATEUR
class ChanteurDAO { 

    // Table des utilisateurs
    private const TABLE = "T_CHANTEURS";

    // Connexion à la base de données
    private $Connection;

    /** 
    * Cette méthode un peu spéciale est le constructeur
    * Elle est exécutée lorsque vous créez un objet UtilisateurDAO
    */ 
    public function __construct() 
    { 
        // Enregistrement du message dans le fichier log
        error_log("ChanteurDAO -> __construct()".PHP_EOL, 3, LOGFILE);

        try {
            // Obtenir une connexion à la base
            // Mémorisation de la connexion dans l'attribut d'instance ($Connection) de la classe
            // Cette connexion est utilisée par les autres méthodes pour envoyer des requêtes
            $hconnection = new Connexion();
            $this->Connection = $hconnection->getConnection();
        }
        // Exception est levée si connexion à la BD impossible
        catch (\Exception $e) {
            // Créer une exception qui sera reçue par la méthode qui a effectué un new de cette classe
            throw new \Exception('Impossible d\'établir la connexion à la BD.');
        }
    } 

    // Vérifier si l'authentification est ok à partir du login et mot de passe passés en paramètre
    // Retourne true si authentification ok SINON false
    public function check_login($login, $password) : bool
    { 
        // Enregistrement du message dans le fichier log
        error_log("ChanteurDAO -> check_login()".PHP_EOL, 3, LOGFILE);

        // Par défaut, le retour de la fonction est positionné à false
        $bRet = false;
        
       // Création d'une requête préparée (prepared statement)
    	$requete = $this->Connection->prepare("SELECT * FROM ".self::TABLE." 
        where email=? and motdepasse=? limit 1");
        
        // Encrypter le mot de passe avec md5
        $password = md5($password);
        //$password = hash('sha256', $password);

        // Enregistrement du message dans le fichier log
        error_log("ChanteurDAO -> check_login() : mot de passe : ".$password , 3, LOGFILE);

        // Exécuter la requête préparée en lui passant une tableau avec le login et le 
        // mot de passe : 1er ? est remplacé par le login, le second ? par le password
        $bret = $requete->execute(array($login, $password));

        // Récupérer le résultat de la requête sour forme d'un tableau
        $chanteur = $requete->fetchAll();

        // SI un utilisateur existe avec ce login et mot de passe
        if (count($chanteur) > 0) {
            
            // Modification du nom de l'ID de session (par sécurité)
            // Par défaut le nom est PHPSESSID
            session_name('myid');
            // Démarrer une session
            // session_start();

            // Sauvegarder le prénom et le nom dans une variable de session nommée prenom_nom
            $_SESSION["prenom_nom"] = ucfirst(strtolower($chanteur[0]["prenom"]))." ".strtoupper($chanteur[0]["nom"]);

            // Positionner à true le booléen retourné par la fonction
            $bRet = true;
        } 

        // Fermer la connexion
        $this->Connection = null;

        // Retourner le booléen : false ou true
        return $bRet;
    }

    // Fonction de création d'un utilisateur
    public function create($chanteur) : bool
    { 
        // Enregistrement du message dans le fichier log
        error_log("ChanteurDAO -> create()".PHP_EOL, 3, LOGFILE);

        //
        // Vérifier si le compte existe déjà
        //

        // Création d'une requête préparée
        $requete = $this->Connection->prepare("SELECT * FROM ".self::TABLE." where email=? limit 1");
        
        // Exécution de la requête
        $requete->execute(array($user->getMail()));

        // Récupérer le résultat de la requête sous forme d'un tableau 
        $result = $requete->fetchAll();

        // Si le compte existe déjà
        if (count($result) > 0)
            // Lever une exception : fin de la méthode
            throw new \Exception('Compte existe.');

        //
        // Création du nouveau compte dans la base de données
        //

        // nom, prenom , email, motdepasse
        // Création d'une requête préparée
        $requete = $this->Connection->prepare("INSERT INTO ".self::TABLE." (nom,prenom,descriptionUser,imageUser,email,motdepasse)
        VALUES (:nom,:prenom,:descriptionUser,:imageUser:email,:motdepasse)");

        // Encrypter le mot de passe avec l'algorithme md5
        $password = md5($chanteur->getMotdepasse());
        // sha256 => 64 bits sha384 => 96 bits sha512 => 128 bits
        //$password = hash('sha256', $user->getMotdepasse());

        // Exécution de la requête : execute() retourne si exécution requête ok sinon false
        $result = $requete->execute(array(
                "nom" => $chanteur->getNom(),
                "prenom" => $chanteur->getPrenom(),
                "descriptionUser" => $chanteur->getDescription(),
                "imageUser" => $chanteur->getImageUser(),
                "email" => $chanteur->getMail(),
                "motdepasse" => $password));

        
        // Fermer la connexion à la BDD
        $this->Connection = null;

        // Retourner le résultat
        return $result;
    }

     // Fonction de suppression d'un utilisateur à partir de son id
     public function deleteChanteur($id)
     {
        // Enregistrement du message dans le fichier log
        error_log("ChanteurDAO -> deleteChanteur()".PHP_EOL, 3, LOGFILE);

        $bRet = true;
        try {
            // Création d'une requête préparée
            $requete = $this->Connection->prepare("DELETE FROM ".self::TABLE." WHERE id = ?");

            // Exécuter la requête
            $bRet = $requete->execute(array($id));
        }
        catch (\Exception $e) {
            $bRet = false;
            // Lever une exception : fin de la méthode
             throw new \Exception('Suppression compte impossible.');
        }

        // Fermer la connexion à la BDD
        $this->Connection = null; 

        // Retourne true si suppression ok sinon false
        return $bRet;
     }

     public function changeChanteurPasswordFromEmail($email,$password)
     {
        // Enregistrement du message dans le fichier log
        error_log("ChanteurDAO -> changeChanteurPasswordFromEmail()".PHP_EOL, 3, LOGFILE);

        $bRet = true;
        $mdp = md5($password);
        try {
            // Création d'une requête préparée
            $requete = $this->Connection->prepare("UPDATE ".self::TABLE." SET motdepasse = ? WHERE email = ?");

            // Exécuter la requête
            $bRet = $requete->execute(array($mdp,$email));
            //printf("%d Row inserted.\n",  $bRet->affected_rows);
        }
        catch (\Exception $e) {
            $bRet = false;
            // Lever une exception : fin de la méthode
            error_log("ChanteurDAO -> changeChanteurPasswordFromEmail() ERROR REQUEST".PHP_EOL, 3, LOGFILE);
             throw new \Exception('Récuperation de compte impossible.');
        }

        // Fermer la connexion à la BDD
        $this->Connection = null; 

        // Retourne true si suppression ok sinon false
        return $bRet;
     }

    // Fonction qui retourne sous forme d'un tableau tous les enregistrements de la table T_UTILISATEUR
    public function findAll() : array{

        // Enregistrement du message dans le fichier log
        error_log("ChanteurDAO -> findAll()".PHP_EOL, 3, LOGFILE);
        
        // Création d'une requête préparée
        $requete = $this->Connection->prepare("SELECT id,nom,prenom,descriptionUser,imageUser FROM ".self::TABLE);

        // Exécution de la requête
        $requete->execute();
        
        // Retourne le résultat de la requête sous forme d'un tableau
        $result = $requete->fetchAll();

        // Tableau des utilisateurs
        $tab_chanteurs = array();

        foreach ($result as $valeur) {
            // Création d'un objet Utilisateur
            $chanteur = new \modele\metier\Utilisateur();

            // Positionner les attributs en utilisant les fonctions setter
            $chanteur->setId($valeur["id"]);
            $chanteur->setNom($valeur["nom"]);
            $chanteur->setPrenom($valeur["prenom"]);
            $chanteur->setDescriptionUser($valeur["descriptionUser"]);
            $chanteur->setImageUser($valeur["imageUser"]);

            // Ajouter l'objet Utilisateur dans le tableau
            $tab_chanteurs[] = $chanteur;

            // Enregistrement du message dans le fichier log
            error_log("ChanteurDAO -> Chanteur : ".$chanteur, 3, LOGFILE);
        }

        // Fermer la connexion à la BDD
        $this->Connection = null; 

        // Retourner le tableau des utilisateurs
        return $tab_chanteurs;
    }
    
    /**  
    * Destructeur, appelé quand l'objet est détruit
    */  
    public function __destruct()  
    {  
        // Enregistrement du message dans le fichier log
        error_log("UtilisateurDAO -> __destruct()".PHP_EOL, 3, LOGFILE);
    }
}

?>