<?php 
namespace modele\service;

// Inclure le fichier des constantes : il contient notamment la constante LOGFILE
//include(__DIR__.'/../../Constantes.php');

class ConcertService { 

    // Référence sur l'objet UtilisateurDao
    private $hConcertDao;

        /** 
    * Cette méthode un peu spéciale est le constructeur
    * Elle est exécutée lorsque vous créez un objet UtilisateurService
    */ 
    public function __construct() 
    {   
        // Enregistrement du message dans le fichier log
        error_log("ConcertService -> __construct()".PHP_EOL, 3, LOGFILE);

        try {
            // Instancier la classe UtilisateurDao : appel du constructeur __construct() 
            // Si problème, la classe UtilisateurDao lève une exception
            $this->hConcertDao = new \modele\dao\ConcertDao();
        }
        // Propagation de l'exception : l'exception est transmise à la méthode appelante
        catch (\Exception $e) {
            throw new \Exception('Impossible d\'établir la connexion à la BD.');
        }
    } 
	
    /**  
    * Destructeur, appelé quand l'objet est détruit
    */  
    public function __destruct()  
    {  
        // Enregistrement du message dans le fichier log
        error_log("ConcertService -> __destruct".PHP_EOL, 3, LOGFILE);	
    }

    public function findAll() : array
    { 
        // Enregistrement du message dans le fichier log
        error_log("ConcertService -> findAll()".PHP_EOL, 3, LOGFILE);

        // Appel de la méthode findAll() de la classe UtilisateurDao
        // Retourne le tableau des utilisateurs
        $results = $this->hConcertDao->findAll();

        // Enregistrement du tableau dans le fichier log
        error_log("ConcertService -> Concerts : ".print_r($results, TRUE), 3, LOGFILE);

        // Retourne le tableau des utilisateurs
        return $results;
    }

    public function createConcert($concert)
    { 
        // Enregistrement du message dans le fichier log
        error_log("UtilisateurService -> createUser()".PHP_EOL, 3, LOGFILE);

        try {
            // Appel de la méthode create() de la classe UtilisateurDao
            // Retourne true si utilisateur créé SINON false
            $bRet = $this->hConcertDao->create($concert);
        }
        // Propagation de l'exception : utilisateur existe déjà
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        // Retourne true si utilisateur créé SINON false 
        return $bRet;
    }

    public function deleteConcert($id)
    { 
        // Enregistrement du message dans le fichier log
        error_log("ConcertService -> deleteUser()".PHP_EOL, 3, LOGFILE);

        try {
            // Appel de la méthode deleteUser() de la classe UtilisateurDao
            // Retourne true si utilisateur créé SINON false
            $bRet = $this->hConcertDao->deleteConcert($id);
        }
        // Propagation de l'exception : suppression impossible
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        // Retourne true si utilisateur a été supprimé 
        return $bRet;
    }

}
?>