
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/png" href="images/Logo_Ideal_Concert_Blanc.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-dark bg-dark sticky-top">
      <div class="container-fluid">
        <a class="navbar-brand" href="../controleur/FrontControleur.php?action=accueil">
        <img src="images/Logo_Ideal_Concert_Blanc.png" alt="Logo" width="35" height="auto" class="d-inline-block align-text-top">
        </a>
        <a class="navbar-brand" href="../controleur/FrontControleur.php?action=accueil">Ideal Concert</a>
        <button
          class="navbar-toggler"
          type="button"
          data-bs-toggle="offcanvas"
          data-bs-target="#offcanvasDarkNavbar"
          aria-controls="offcanvasDarkNavbar"
        >
          <span class="navbar-toggler-icon"></span>
        </button>
        <div
          class="offcanvas offcanvas-end text-bg-dark"
          tabindex="-1"
          id="offcanvasDarkNavbar"
          aria-labelledby="offcanvasDarkNavbarLabel"
        >
          <div class="offcanvas-header">
            <h5 class="offcanvas-title" id="offcanvasDarkNavbarLabel">Menu</h5>
            <button
              type="button"
              class="btn-close btn-close-white"
              data-bs-dismiss="offcanvas"
              aria-label="Close"
            ></button>
          </div>
          <div class="offcanvas-body">
            <ul class="navbar-nav justify-content-end flex-grow-1 pe-3">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="../controleur/FrontControleur.php?action=accueil">Accueil</a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="../controleur/FrontControleur.php?action=list_chanteurs">Les chanteurs</a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="../controleur/FrontControleur.php?action=list_concerts">Concert à venir</a>
              </li>
              <li class="nav-item dropdown">
                <a
                  class="nav-link active dropdown-toggle show"
                  href="#"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="true"
                >
                  Ideal Concert
                </a>
                <ul
                  class="dropdown-menu dropdown-menu-dark show"
                  data-bs-popper="static"
                >
                  <li><a class="dropdown-item" href="../controleur/FrontControleur.php?action=lieu">Le lieu</a></li>
                  <li><a class="dropdown-item" href="../controleur/FrontControleur.php?action=contact">Contact</a></li>
                </ul>
              </li>
              <li class="nav-item">
                <a class="nav-link active" href="../controleur/FrontControleur.php?action=deconnexion">Déconnexion</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>
</body>
</html>