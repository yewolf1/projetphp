<!doctype html>
<?php require_once("../Autoloader.php");
session_name('myid');
session_start(); ?>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/png" href="https://img.icons8.com/cotton/2x/checkmark.png">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/animation.css">
    <link rel="stylesheet" type="text/css" href="css/theme.css">
    <title>Liste des chanteurs</title>
  </head>
  <?php include 'navbar.php' ?>
  <body class="text-white">
        <div class="text-center mt-5">
            <h1 class="display-2 fs-1 text-center text-dark">Liste des chanteurs</h1>
        </div>     
    <div class="container d-flex justify-content-around flex-wrap my-5">
        <?php 
        $tableauChanteur=$_SESSION['tableauChanteur']; 
        foreach ($tableauChanteur as $chanteur) {
            ?> 
            <div class="card rounded-4 bg-dark m-3" style="width: 18rem;">
                <img src="data:image/jpeg;base64,<?= base64_encode($chanteur->getImageUser()) ?>" class="rounded-bottom rounded-4 card-img-top" alt="<?= $chanteur->getPrenom() ?> <?= $chanteur->getNom() ?> image">
                <div class="card-body"> 
                    <h5 style="color: #25B8C5" class="card-title text-center"><?= $chanteur->getPrenom() . " " . $chanteur->getNom() ?></h5>
                    <p class="card-text"><?= $chanteur->getDescriptionUser() ?></p>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
    <div class="text-center mt-5"><a class="backButton boxbut" href="../controleur/FrontControleur.php?action=accueil">Revenir à la page d'accueil</a></div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function () {
        $('#table_user').DataTable();
    });
    </script>
</body>
<?php include 'footer.php' ?>
</html>