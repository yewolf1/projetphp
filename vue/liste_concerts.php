<!doctype html>
<?php require_once("../Autoloader.php");
session_name('myid');
session_start(); 
?>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/png" href="https://img.icons8.com/cotton/2x/checkmark.png">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/animation.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <title>Liste des concerts</title>
  </head>
  <?php include 'navbar.php' ?>
  <body style="background: #fff" class="text-white">
    <h1 class="display-2 text-center text-dark" style="color: #343a40">Liste des concerts à venir</h1>
    <div class="container justify-content-center mt-5">
    <?php 
    $tableConcert=$_SESSION['tableauConcert']; 
        foreach ($tableConcert as $concert) { ?>
            <div class="containerConcert w-100 d-flex" style="background: #343a40; border-radius: 15px">
                <img src="data:image/jpeg;base64,<?= base64_encode($concert->getImageConcert()) ?>" class="imgConcert">
                <div class="h-100 w-100 text-white">
                    <h3 class="h3Concert mb-3" style="color: #25B8C5"><?= $concert->getSingerConcert() ?></h3>
                    <h5 class="h5Concert mb-3" style="color: #25B8C5"><?= "Le " . $concert->getSingerConcert(); ?></h5>
                    <p class="pConcert"><?= $concert->getDescriptionConcert(); ?></p>
                    <button type="button" class="btn" style="background: #25B8C5">Réservez votre place !</button>
                </div>
            </div>
        <?php }?>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <!-- jQuery Datatable js -->
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
    <script src="main.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function () {
        $('#table_user').DataTable();
    });
    </script>
</body>
<?php include 'footer.php' ?>
</html>