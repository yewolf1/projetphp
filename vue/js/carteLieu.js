var map = L.map('carteJS').setView([43.6244, 1.43], 13);

L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

L.marker([43.6244, 1.43]).addTo(map)
    .bindPopup("<p style='text-align: center;' > Ici</p>"+
    "<img src='images/Caroussel_img_1.jpg' style='width: 8vw !important; margin: 0 auto;'>")
    .openPopup();


