const gliderA = new Glider(document.querySelector('.glider'), {
    slidesToScroll: 2,
    slidesToShow: 2,
    duration: 0.5,
    interval: 2000,
    startItem: 0,
    draggable: true,
    dots: '.dots',
    arrows: {
      prev: '.glider-prev',
      next: '.glider-next'
    }
});

gliderAutoplay(gliderA , {interval : 3000});