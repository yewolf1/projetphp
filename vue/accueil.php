<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/png" href="images/Logo_Ideal_Concert_Blanc.png">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/glider-js@1/glider.min.css">
    <link rel="stylesheet" type="text/css" href="css/animation.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <title>Accueil</title>
  </head>
  <?php include 'navbar.php' ?>
  <body style="background: white" class="text-black">

    <div class="container-md">
      <div class="row">
        <div class="col-md-8">

          <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img src="images/Caroussel_img_1.jpg" class="d-block w-100" alt="Image salle de concert">
              </div>
              <div class="carousel-item">
                <img src="images/Caroussel_img_2.jpg" class="d-block w-100" alt="Photo du rapeur JUL en concert">
              </div>
              <div class="carousel-item">
                <img src="images/Caroussel_img_3.jpg" class="d-block w-100" alt="Photo du chanteur Calogero en concert">
              </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Next</span>
            </button>
          </div>
        </div>


        <div class="col-md-4">
          <ul class="list-group">
            <li class="list-group-item">Liste des chanteurs</li>
            <li class="list-group-item">Bob Marley</li>
            <li class="list-group-item">Stevie Wonder</li>
            <li class="list-group-item">Jul</li>
            <li class="list-group-item">Rihanna</li>
            <li class="list-group-item"><a href="../controleur/FrontControleur.php?action=list_chanteurs">Plus...</a></li>
          </ul>

          <ul class="list-group2">
            <li class="list-group-item">Liste des concerts</li>
            <li class="list-group-item"> 18/09/2022 </li>
            <li class="list-group-item"> 22/09/2022 </li>
            <li class="list-group-item"> 24/09/2022 </li>
            <li class="list-group-item"> 07/10/2022 </li>
            <li class="list-group-item"><a href="../controleur/FrontControleur.php?action=list_concerts">Plus...</a></li>
          </ul>
        </div>
        
      </div>
    </div>

    <div class="Desc_accueil">
      <p> Ideal Concert est né en 1988 dans le but de doter la commune de Saint-Orens de Gameville d’un outil culturel pour abriter et centraliser la vie culturelle en un seul lieu. L’objectif était de faire rayonner la culture Saint-Orennaise dans la ville, dans la métropole toulousaine et dans le Lauragais.</b></p>
      <p> En 2022, et par délibération le Conseil Municipal de Saint-Orens de Gameville décide de transformer la salle Ideal Concert en SPL, Société Publique Locale, afin de mieux répondre aux enjeux d’exploitation d’une salle de spectacle d’aujourd’hui. Cette transformation est le début d’un grand chantier de restructuration de l’établissement. Dans cette optique, la ville a engagé un partenariat avec 4 communes limitrophes qui ont rejoint cette ambition de faire rayonner la culture sur tout le territoire et de la rendre accessible au plus grand nombre.</p></b>

      <p> Vers d’autres univers, Ideal Concert a la volonté d’être un lieu à vivre qui favorise la découverte, les expériences et la rencontre ; un lieu qui porte la culture et la partage avec tout le monde : les passionné·e·s, les curieux·ses mais aussi celles et ceux qui pensent que la culture n’est peut-être pas pour elles ou eux.</p></b>

      <p> Pour amplifier ses ambitions, Ideal Concert va être rénové dans un futur proche. Ce grand projet sera accompagné par un chantier de préfiguration pour essayer d’imaginer collectivement ce que sera l’Ideal Concert de demain. Cette transformation progressive du lieu est plus qu’une mutation : c’est un projet collectif dans lequel les publics et les habitants seront largement associés.</b></p>
    </div>

    
    <div class="slider-wrap" id="slider">
      <h1> Les festivals à venir </h1>

            <div class="products glider">
              <!-- Element 1 -->
                <div class="festivals" >
                    <img src ="images/caroussel_accueil/festivalrose.png" alt="Logo festival rose" >
                    <div class="festival_Text">
                        <p>ROSE FESTIVAL </br></p>
                        <p>Le 05/10/2022 </br></p>
                    </div>
                </div>
              <!-- Element 2 -->
                <div class="festivals">
                  <img src ="images/caroussel_accueil/indisciplinees.jpg" alt="Logo festival les indisciplinées" >  
                  <div class="festival_Text">
                        <p>
                          INDISCIPLINEES </br>
                          Le 20/10/2022 </br>
                        </p>
                    </div>
                </div>
              <!-- Element 3 -->  
                <div class="festivals " >
                  <img src ="images/caroussel_accueil/pinkparadise.jpg" alt="Logo festival Pink paradise" >
                  <div class="festival_Text">
                        <p>
                          PINK PARADISE </br>
                          Le 08/11/2022 </br>
                        </p>
                    </div>
                </div>
              <!-- Element 4 -->
                <div class="festivals">
                  <img src ="images/caroussel_accueil/primmassy.jpg" alt="Logo festival Primmassy" >
                  <div class="festival_Text">
                        <p>
                          PRIMMASSY </br>
                          Le 19/11/2022 </br>
                        </p>
                    </div>
                </div>
              <!-- Element 5 -->
                <div class="festivals">
                  <img src ="images/caroussel_accueil/insolent.jpg" alt="Logo festival Insolent" >
                  <div class="festival_Text">
                        <p>
                          INSOLENT </br>
                          Le 05/12/2022 </br>
                        </p>
                    </div>
                </div>
              <!-- Element 6 -->
                <div class="festivals">
                  <img src ="images/caroussel_accueil/fiestasud.jpg" alt="Logo festival Fiesta sud" >
                  <div class="festival_Text">
                        <p>
                          FIESTA SUD </br>
                          Le 30/12/2022 </br>
                        </p>
                    </div>
                </div>
              <!-- Element 7 -->
              <div class="festivals">
                  <img src ="images/caroussel_accueil/RioLoco.jpg" alt="Logo festival Rio loco" >
                  <div class="festival_Text">
                        <p>
                          Rio Loco </br>
                          Le 03/01/2023 </br>
                        </p>
                    </div>
                </div>
              <!-- Element 8 -->
              <div class="festivals">
                  <img src ="images/caroussel_accueil/festival-air.jpg" alt="Logo festival 3 air" >
                  <div class="festival_Text">
                        <p>
                          FESTIVAL 3 AIR </br>
                          Le 08/01/2023 </br>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        
    </div>


    <div class="partenaires">
      <h1>Nos partenaires</h1></br>
      <div class="images_partenaires">
        <img src ="images/Logo_Occitanie.svg.png" alt="Logo La région">
        <img src ="images/coca-cola-logo.jpg" alt="Logo La région">
        <img class="logo_tlsM" src ="images/Toulouse_Métropole.svg.png" alt="Logo metropole toulouse">
        <img class="logo_CM" src ="images/credit-mutuel.png" alt="Logo crédit mutuel">
      </div>

    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    
    <!-- GLIDER JS -->
    <script src="https://cdn.jsdelivr.net/npm/glider-js@1/glider.min.js"></script>
    <script src="js/glider-autoplay.min.js"></script>
    <script src="js/accueil_caroussel.js"></script>

  </body>
    <?php include 'footer.php' ?>
</html>

