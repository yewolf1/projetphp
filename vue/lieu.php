<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/png" href="images/Logo_Ideal_Concert_Blanc.png">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/animation.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!-- leafletjs -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.2/dist/leaflet.css"
     integrity="sha256-sA+zWATbFveLLNqWO2gtiw3HL/lh1giY/Inf1BJ0z14="
     crossorigin=""/>

    <title>Lieu</title>
  </head>
  <?php include 'navbar.php' ?>
  <body style="background: white" class="text-black">
    <h1 class="display-2 text-dark" style="margin-left: 8%;padding: 1%;"> Ou nous trouver </h1>
    <div class="container">
        <div class="row">
         <div id="carteJS">Si vous voyez ce message alors la carte ne s'est pas éxécutée correctement</div>
        </div>
    </div>

    <div class="Desc_accueil">
      <h3 class="display-2 text-dark">Horaires</h3>
      <p> Horaires d'ouverture : du lundi au vendredi de 9h à 12h30 et de 14h à 17h.
      <br/>Horaires des activités hors vacances scolaires : du lundi au vendredi de 9h à 22h et le samedi matin de 8h30 à 13h.</b></p>
      
      <h3 class="display-2 text-dark">Description</h3>
      <p>Situé dans le quartier des Minimes, le Centre culturel - Théâtre des Mazades est un espace de 5000 m² 
        équipé d’une salle de spectacle de 500 places, d’un studio de danse, d’un petit théâtre, 
        d’un espace d’exposition, d’un dojo et de salles d’activités.
      </p></b>

      <p> La programmation est pluridisciplinaire, ancrée dans une écriture contemporaine, 
        et favorise l’émergence de la scène locale.
      </p></b>

      <p> Également lieu de pratique amateur (théâtre, danse, cirque, arts plastiques, judo, forme et bien-être..),
         d’actions culturelles et de médiations, le centre contribue à l’éducation artistique et 
         culturelle des publics, dans une exigence d’accessibilité et de qualité.
      </b></p>
      <p>En s’appuyant sur de nombreux partenariats associatifs et institutionnels, ainsi que des actions de proximité hors les murs à la rencontre des habitants, le centre culturel s’implique fortement dans l’animation de la vie locale.</p>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <!-- Leaflet's  -->
    <script src="https://unpkg.com/leaflet@1.9.2/dist/leaflet.js"
     integrity="sha256-o9N1jGDZrf5tS+Ft4gbIK7mYMipq9lqpVJ91xHSyKhg="
     crossorigin=""></script>
    <script src="js/carteLieu.js"></script>
  </body>
    <?php include 'footer.php' ?>
</html>
