<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/png" href="images/Logo_Ideal_Concert_Blanc.png">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/glider-js@1/glider.min.css">
    <link rel="stylesheet" type="text/css" href="css/animation.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <title>Accueil</title>
  </head>

  <?php include 'navbar.php' ?>

  <body style="" class="text-white bg">
    <div class="container d-flex justify-content-center mt-5">
        <div class="row w-70" id="glass" style="margin-bottom: 40px !important; ">
            <!--  bandeau logo -->
            

            <!--  -->
            <div class="col-md text-center contenu1">
                <h1 class="display-2 text-center">Contacter nous</h1>
                <form class="form-inlin mt-4" method="" action="" style="display: flex;
    flex-direction: column;
    align-items: center;">
                    <div class="form-group">
                        <input type="" class="form-control " name="nom" placeholder="Votre nom" required>
                    </div>
                    <div class="form-group">
                        <input type="" class="form-control " name="prenom" placeholder="Votre prenom" required>
                    </div>

                    <textarea id="story" name="story" rows="10" cols="53" class="form-control area1">Le message</textarea>

                    <button type="submit" name="envoi" class="buttoonf px-3 py-1 font-weight-bold boxbut" style="cursor: pointer;">Valider</button>
                </form>
                

                <div style="text-align: center;">

                   
            </div>          
        </div>
        
    </div>
                </div>

    
   
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    
    <!-- GLIDER JS -->
    <script src="https://cdn.jsdelivr.net/npm/glider-js@1/glider.min.js"></script>
    <script src="js/glider-autoplay.min.js"></script>
    <script src="js/accueil_caroussel.js"></script>
    </body>

    <?php include 'footer.php' ?>
</html>
